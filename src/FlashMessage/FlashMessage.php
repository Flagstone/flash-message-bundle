<?php declare(strict_types=1);
/** *****************************************************************************************************************
 *  FlashMessage.php
 *  *****************************************************************************************************************
 *  @copyright 2019 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  *****************************************************************************************************************
 *  Created: 2022/01/25
 *  ***************************************************************************************************************** */
namespace Flagstone\FlashMessageBundle\FlashMessage;

use Flagstone\FlashMessageBundle\FlashMessage\Exception\FlashBagNotAvailableException;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class FlashMessage
{
    /**
     *  @var FlashBagInterface
     *  ------------------------------------------------------------------------------------------------------------- */
    private FlashBagInterface $flashBag;

    const FLASH_DANGER  = 'danger';
    const FLASH_WARNING = 'warning';
    const FLASH_SUCCESS = 'success';
    const FLASH_INFO    = 'info';

    /** *************************************************************************************************************
     *  FlashMessage constructor.
     *  @param RequestStack $requestStack
     *  @throws FlashBagNotAvailableException
     *  ************************************************************************************************************* */
    public function __construct(RequestStack $requestStack)
    {
        if (null !== $requestStack->getCurrentRequest()) {
            $this->flashBag = $requestStack->getCurrentRequest()->getSession()->getFlashBag();
        } else {
            $this->flashBag = null;
        }
    }

    /** *************************************************************************************************************
     *  Add a message in the flash bag
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $level
     *  @param string $message
     *  @param array $options
     *  ************************************************************************************************************* */
    public function add(string $level, string $message, array $options = []): void
    {
        if (null !== $this->flashbag) {
            if (count($options) > 0) {
                $this->flashBag->add(
                    $level,
                    vsprintf(
                        $message,
                        $options
                    )
                );
            } else {
                $this->flashBag->add(
                    $level,
                    $message
                );
            }
        }
    }

    /** *************************************************************************************************************
     *  Add a danger message in the flash bag
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $message
     *  @param array $options
     *  ************************************************************************************************************* */
    public function danger(string $message, array $options = []): void
    {
        $this->add(self::FLASH_DANGER, $message, $options);
    }

    /** *************************************************************************************************************
     *  Add a success message in the flash bag
     *  -------------------------------------------------------------------------------------------------------------
     * @param string $message
     * @param array $options
     *  ************************************************************************************************************* */
    public function success(string $message, array $options = []): void
    {
        $this->add(self::FLASH_SUCCESS, $message, $options);
    }

    /** *************************************************************************************************************
     *  Add a warning message in the flash bag
     *  -------------------------------------------------------------------------------------------------------------
     * @param string $message
     * @param array $options
     *  ************************************************************************************************************* */
    public function warning(string $message, array $options = []): void
    {
        $this->add(self::FLASH_WARNING, $message, $options);
    }

    /** *************************************************************************************************************
     *  Add an info message in the flash bag
     *  -------------------------------------------------------------------------------------------------------------
     * @param string $message
     * @param array $options
     *  *************************************************************:q!************************************************ */
    public function info(string $message, array $options = []): void
    {
        $this->add(self::FLASH_INFO, $message, $options);
    }
}
